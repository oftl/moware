FROM alpine as moware-base

WORKDIR /root

RUN apk update \
 && apk update \
 && apk add \
        python3 \
        py3-virtualenv \
        py3-pytest \
        git

FROM moware-base as moware-break-down

# XXX keep in mind: this no so clever
# dockerd won't recognise updated sources, since this is just a RUN command
#
RUN git clone https://gitlab.com/oftl/moware.git \
 && cd moware \
 && virtualenv local \
 && source ./local/bin/activate \
 && pip install --requirement requirements.txt

CMD ["pytest", "/root/moware"]
