import unittest

from main import break_down


class TestBreakDown(unittest.TestCase):
    def test_break_down(self):
        self.assertEqual(break_down(500), 1, "expected 1")
        self.assertEqual(break_down(81), 4, "expected 4")
        self.assertEqual(break_down(42), 3, "expected 3")
        self.assertEqual(break_down(581), 5, "expected 5")
        self.assertEqual(break_down(1334), 8, "expected 8")
        self.assertEqual(break_down(184827), 374, "expected 374")

        self.assertEqual(break_down(0), 0, "expected 0")

        with self.assertRaises(Exception):
            break_down(1.1)

        with self.assertRaises(Exception):
            break_down(-1)
