from typing import Dict, List, NewType

Wallet = NewType("Wallet", List[Dict[str, int]])


def break_down(amount: int, debug: bool = False) -> int:
    """
    breaking down the given amount (currency: euro, whole number, no subunits/cent)
    into the smallest possible number of notes and coins

    :param int amount: amount to break down
    :param bool debug: activate some helpful output
    :return: smallest possible number of notes and coins
    :rtype: int
    """

    if debug:
        print("go ...")

    if not amount >= 0:
        raise Exception("expected an amount >= 0")

    if not isinstance(amount, int):
        raise Exception("expected whole number")

    # normalise to cents
    amount_normalised: int = amount * 100

    # count number of notes/coins
    count: int = 0

    wallet: Wallet = Wallet([])

    # denominations >= 1 euro, in cents
    denominations: List[int] = [
        500_00,
        200_00,
        100_00,
        50_00,
        20_00,
        10_00,
        5_00,
        2_00,
        1_00,
    ]

    # how many of the larges possible denomination can we use?
    # if it is none, try the next smaller one
    #
    for denomination in denominations:
        # we're already done
        if amount_normalised == 0:
            break

        times: int = amount_normalised // denomination
        remainder: int = amount_normalised % denomination

        if times:
            count += times
            amount_normalised = remainder

            wallet.append(
                {
                    "times": times,
                    "denomination": denomination,
                }
            )

    if amount_normalised != 0:
        raise Exception("this is bad and should not be possible")

    if debug:
        for w in wallet:
            print(f"must fetch {w['times']} pieces of {w['denomination'] // 100}")

    if debug:
        print("done.")

    return count
