## Use Docker

There's no need to checkout this repository. Just run the two commands below to build a Docker image and run a few simple tests.

```
docker build --tag moware .
docker run --rm moware
```

You'll find actual source code in `moware/main.py`

## Try it out manually

### Fetch sources

```
git clone https://gitlab.com/oftl/moware.git
```

### Install dependencies in a virtual env

```
cd moware
virtualenv local
source ./local/bin/activate
pip install --requirement requirements.txt
```

### Run some tests

```
pytest
```

### Use pre-commit


Use pre-commit's various hooks:

```
pre-commit run --all-files
```
